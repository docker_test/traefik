docker run -d \
--name traefik \
-p 443:443 \
-p 80:80 \
-p 8080:8080 \
-v $PWD/traefik.toml:/traefik.toml \
-v $PWD/certs:/certs \
-v /var/run/docker.sock:/var/run/docker.sock \
--net=web \
traefik:v1.7.2
